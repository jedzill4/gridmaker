import Tkinter as tk

import random
import time
import sys,os

def EPrint(msg):
    sys.stderr.write(msg)
    sys.stderr.flush()
    


class BError(Exception):
    """Clase base de exceciones"""

class InputError(BError):
    """Error en entrada de columnas  o filas"""

class AreaError(BError):
    """Error en tamanio grilla"""
try:
    rows = input("numero de rows: ")
    cols = input("numero de colsumnas: ")

    if rows <= 1:
        raise AreaError
    if cols <= 1:
        raise AreaError

    if (rows+cols - int(rows)-int(cols)) is not 0:
        raise InputError

except AreaError:
    EPrint("Tamano de grilla invalido, debe ingresar numeros enteros mayores que 1\n")
    sys.exit(1)
except InputError:
    EPrint("Debe ingresar numeros enteros\n")
    sys.exit(1)
except NameError:
    EPrint("Debe ingresar numeros enteros\n")
    sys.exit(1)

WIDTH = 10*cols
HEIGHT = 10*rows

## create the matrices
#cell = [[0 for row in range(-1,filas+1)] for col in range(-1,col+1)]
#live = [[0 for row in range(-1,filas+1)] for col in range(-1,col+1)]
#temp = [[0 for row in range(-1,filas+1)] for col in range(-1,col+1)]
#
#
#def callback(event):
#    canvas = event.widget
#    x = canvas.canvasx(event.x)
#    y = canvas.canvasy(event.y)
#    item= canvas.find_closest(x, y)
#    canvas.itemconfig(item, fill = 'green')
#    #if live[x][y]==1:
#    #    canvas.itemconfig(cell[x][y], fill="black")
#    #if live[x][y]==0:
#    #    canvas.itemconfig(cell[x][y], fill="green")
#    print('click at %i %i' %(x,y))
#
## process and draw the next frame
#def frame():
#    draw()
#    root.after(100, frame)
#
## load the initial data
#def load():
#    for y in range(-1,filas+1):
#        for x in range(-1,col+1):
#            live[x][y] = 0
#            temp[x][y] = 0
#            cell[x][y] = canvas.create_rectangle((x*10, y*10, x*10+10, y*10+10), outline="gray", fill="black")
#
#
## Draw all cells
#def draw():
#    for y in range(filas):
#        for x in range(col):
#            if live[x][y]==0:
#                canvas.itemconfig(cell[x][y], fill="black")
#            if live[x][y]==1:
#                canvas.itemconfig(cell[x][y], fill="green")
#
## main loop
#root = Tk()
#root.title("Gridmaker")
#canvas = Canvas(root, width=WIDTH, height=HEIGHT, highlightthickness=0, bd=0, bg='black')
#canvas.bind("<Button-1>",callback)
#canvas.pack()
#load()
#frame()
#root.mainloop()

def callback():
    print 'asdasd'
class App(tk.Tk):
    def __init__(self):
        tk.Tk.__init__(self)

#menu bar
        self.menubar = tk.Menu(self)
        self.create_menu()
        self.config(menu=self.menubar)
        self.frame = tk.Frame(self, width=600, height=480, background="gray")
        self.frame.pack(side="top", fill="both", expand=True)
        self.canvas = tk.Canvas(self.frame, width=WIDTH, height=HEIGHT, highlightthickness=0, bd=0, bg='black')
        self.canvas.bind("<Button-1>",self.changecolor)
        self.populate()
        self.canvas.pack()

    def create_menu(self):
        filem = tk.Menu(self.menubar,tearoff=0)
        filem.add_command(label="New",command=callback)
        filem.add_separator()
        filem.add_command(label="Exit",command=self.quit)
        self.menubar.add_cascade(label="File",menu=filem)
    def populate(self):
        for i in range(-1,rows+1):
            for j in range(-1,cols+1):
                self.canvas.create_rectangle((i*10, j*10, i*10+10, j*10+10), outline="gray", fill="black")

    def changecolor(self,event):
        canvas = event.widget
        x = canvas.canvasx(event.x)
        y = canvas.canvasy(event.y)
        item= canvas.find_closest(x, y)
        canvas.itemconfig(item, fill = 'green')


        
app = App()
app.mainloop()
